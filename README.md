# Beeefeeder

Inspired by Paul Stamets (both [1] [2]) I started making a beefeeder, to cluster a mix of ideas:

## short term
* feed bees sugarwater
* collect images
* make this as a DIY kit to help people, kids!, the make this an introduction to open source and hardware.

## mid term
Make a [mishroom mix](https://www.youtube.com/watch?v=TZPkCozuqM8 ) to add to the sugar water to help the bees against viri en bacteria, containing :

* Red Belted Polyfore,  Fomitopsis Pinicola
* Amadou, Fomes Fomentarius
* Red Reishi, Ganoderma Lucidum

# longer term 
* buildup image collection over time
* federate image collection
* build models for deep learning using known collections [3]
* use models to impovrove collecting results
* Create data sets overtime

##
* M6 thread end  60 cm or 100cm
* 4x M6 nut
* PLA or other filamnet for you 3D printer
* empty bottle that fits....

optional:
* usb-webcam endoscoop max 7mm [4]
* pi camera
* raspberry-pi 
* power over ethernet (injector and ejector) to 5 Volt, i use 802.3af or 802.3at 

software 
* openscad [5] , only if you want adjust the printed parts
* cura [6] 


## tools
* 3D-printer
* 10mm fork spanner
* iron saw

# Step to build
 print the parts in de 3D folder
* [beefeeder2 ](https://gitlab.com/MVijn/beefeeder/-/blob/master/3D/beefeeder2.stl)
* [bottle_ring ](https://gitlab.com/MVijn/beefeeder/-/blob/master/3D/bottle_ring.stl)

[](url)

optional:
* [bf2_extender.stl](https://gitlab.com/MVijn/beefeeder/-/blob/master/3D/bf2_extender.stl) 
* camera mounts pick usb / pi / add your own
 * 
* cut the thread end in 2 equal prieces

Put it together like [this] (https://gitlab.com/MVijn/beefeeder/-/blob/master/3D/images/signal-2021-01-13-160442_004.jpeg)


##

Fill the bottle with water and sugar. 

### urls
* [1] https://nl.wikipedia.org/wiki/Star_Trek:_Discovery
* [2] https://nl.wikipedia.org/wiki/Paul_Stamets_(mycoloog)
* [3] https://bioportal.naturalis.nl/
* [4] http://letmegooglethat.com/?q=usb-webcam+endoscoop+7mm
* [5] https://www.openscad.org/
* [6] https://ultimaker.com/nl/software/ultimaker-cura
