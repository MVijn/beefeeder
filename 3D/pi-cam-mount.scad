
//picam();
//mount();
cap();
//extender();

$fn=50;
module extender(){
    difference(){
    minkowski(){
        
           cube([35,20,10]);
           cylinder(d=10,h=1);
            }
            
    translate([5,5,-1]) minkowski(){
        
           cube([25,10,30]);
           cylinder(d=10,h=1);
            }
    translate([-2,10,-1]) cylinder(d=3.5,h=20); 
    translate([37,10,-1]) cylinder(d=3.5,h=20);

        } 
    
}

% translate([-2,10,-1]) cube([39,10,3]);

//24x25.5x3.7
module picam(){
    union(){
    cube([26,26,3.7]);
    translate([9,7,0]) cube([8,8,30]);
    translate([9,7,0]) cube([8,18,5]);
    translate([20,20,0]) cube([4,4,5]); 
    translate([5,-4,0.9]) cube([16,5,2]);   
    }
}



    
module mount(){
    difference(){
         minkowski(){
        
           cube([35,20,3]);
           cylinder(d=10,h=1);
            }   
        translate([5,22,5.5]) rotate([180,0,0]) picam();
        translate([-2,10,-1]) cylinder(d=3.5,h=20); 
        translate([37,10,-1]) cylinder(d=3.5,h=20);
        }
    }
    
module cap(){
    difference(){
    minkowski(){
        
           translate([2,2,0])cube([35,23,2]);
           cylinder(d=10,h=1);
            }
   /*         
    translate([2,2,2.5]) minkowski(){
        
           cube([35,20,3]);
           cylinder(d=10,h=1);
            }
            */
    translate([0,14,-1]) cylinder(d=3.5,h=20); 
    translate([39,14,-1]) cylinder(d=3.5,h=20);
    translate([5,0,1]) cube([29,11,3]); 
        }
    }   