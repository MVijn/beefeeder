
base();

$fn=100;

//% translate([15.5,37,40]) cube([39,10,3]);

module base(){
    
    difference(){
        union(){
         minkowski(){
            cube([70,20,10]);
            cylinder(d=10,h=1);
            } 
     minkowski(){
            translate([20,30,0]) cube([30,20,30]);
            cylinder(d=10,h=1);
            } 
          translate([35,10,0]) cylinder(d=40,h=30);  
          translate([35-19.5,40,20]) cylinder(d=6,h=11); 
          translate([35+19.5,40,20]) cylinder(d=6,h=11); 
          }
       translate([35,10,2]) cylinder(d=30,h=40);  
       translate([0,10,-1]) cylinder(d=6.5,h=40);
       translate([70,10,-1]) cylinder(d=6.5,h=40);
       translate([35-8,16,5]) cube([16,30,16]);
       translate([10-8,32,5]) cube([70,16,16]);
        minkowski(){
            translate([23,33,5]) cube([24,14,35]);
            cylinder(d=10,h=1);
            }
       #   translate([35-19.5,40,19]) cylinder(d=3,h=14); 
       #   translate([35+19.5,40,19]) cylinder(d=3,h=14);              }
  // translate([-.5,12,-1]) cylinder(d=3.5,h=20); 
  //  translate([29.5,12,-1]) cylinder(d=3.5,h=20); 
       } 
    