

mount();
$fn=150;

//% translate([-2,10,-1]) cube([39,10,3]);

mic_angle=20;

    
module mount(){
        difference(){
        union(){
         minkowski(){       
           cube([35,20,14]);
           cylinder(d=10,h=1);
            } 
            translate([17.5,9,6]) rotate([mic_angle,0,0]) cylinder(d=36,d1=34,h=20);
        }
    translate([17.5,9,6])rotate([mic_angle,0,0])cylinder(d=31,d1=29,h=22);  
    translate([17.5,10,-3]) cylinder(d=25,h=14); 
    //    translate([17.5,10,-1]) cylinder(d=7,h=20); 
        translate([-2,10,-1]) cylinder(d=3.5,h=20); 
        translate([37,10,-1]) cylinder(d=3.5,h=20);
        translate([-2,10,10]) cylinder(d=6,h=20); 
        translate([37,10,10]) cylinder(d=6,h=20);
     
       translate([17.5,10,10]) rotate([90,0,0])  cylinder(d=3,h=20);
        }
    }
    
   